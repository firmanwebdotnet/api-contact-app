const express    = require('express');
const bodyParser = require('body-parser');
const app        = express();
const mysql      = require('mysql');
const cors       = require('cors')
//parse json
app.use(bodyParser.json());

//create DB Connection
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'contact'
});

//confg CORS
// const corsOptions = {
//     origin: 'http://localhost:3000/',
//     optionsSuccessStatus: 200
// }

// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });

 app.use(cors())

//connection to DB
conn.connect((err) => {
    if(err) throw err;
    console.log('MYSQL connecteed...');
});

//view data from DB
 app.get('/listcontact',(req, res, next) => {
    let sql = conn.query("select * from contact_app order by id desc", (err, result) => {
        if(err) throw err;
        // res.send(JSON.stringify([{"status": 200, "Message": 'data tampilkan semua', "response": result}]));
        res.json(result);
    });
 });

 //view data per ID
 app.get('/listcontact/:id', (req, res, next) => {
    let sql =  conn.query("select * from contact_app where id ="+req.params.id, (err, result) => {
        if(err) throw err;
        res.json(result);  
    });
 });

 //tambah data
 app.post('/addcontact', (req, res, next) => {
     let tgl = new Date();
    let data = {
        nama: req.body.nama,
        email: req.body.email,
        nohp: req.body.nohp,
        insert_date: tgl
    }
    let sql = conn.query(`insert into contact_app set ?;`, [data], (err, result) => {
        if(err) throw err;
        res.send(JSON.stringify([{"status": 200, "message": 'data berhasil ditambahkan'}]));
    });
 });

//hapus data
app.delete('/contact/:id', (req, res, next) => {
    // let id = req.body.id
    // let cekData = conn.query(`select count(*) from contact_app where id = ${req.params.id}`, (err, result) => {
    //     res.json(result);
    // });
    
    let sql = conn.query(`delete from contact_app where id = ${req.params.id}`, (err, result) => {
        if(err) throw err;
        res.send(JSON.stringify([{"status": 200, "message": 'data berhasil dihapus'}]));
    });
});
//server listening
app.listen(3000,() => {
    console.log('Server started on port 3000...');
});