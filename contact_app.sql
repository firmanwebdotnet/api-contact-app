-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 07, 2021 at 04:56 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contact`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact_app`
--

CREATE TABLE `contact_app` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `nohp` varchar(20) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_app`
--

INSERT INTO `contact_app` (`id`, `nama`, `email`, `nohp`, `insert_date`) VALUES
(1, 'Lalita Handayani S.Farm', 'nainggolan.simon@gmail.co.id', '(+62) 323 5896 8632', '2021-06-05 21:18:36'),
(2, 'Jasmin Riyanti S.Kom', 'tami64@yahoo.com', '0322 5812 7203', '2021-06-05 21:18:36'),
(3, 'Maria Queen Nasyidah', 'padmasari.ian@safitri.id', '(+62) 536 2265 3030', '2021-06-05 21:18:36'),
(4, 'Carla Chelsea Safitri S.E.I', 'zamira22@yahoo.co.id', '0271 7031 307', '2021-06-05 21:18:36'),
(5, 'Lala Fathonah Zulaika S.Sos', 'kariman.mayasari@gmail.com', '0943 9651 2980', '2021-06-05 21:18:36'),
(6, 'Paramita Aryani', 'prayogo.puspasari@winarsih.ac.id', '(+62) 27 3889 4701', '2021-06-05 21:18:36'),
(7, 'Suci Lailasari', 'mulyono.saptono@gmail.com', '0892 504 581', '2021-06-05 21:18:36'),
(8, 'Najam Pradipta', 'jlestari@marbun.biz', '(+62) 24 4778 5344', '2021-06-05 21:18:36'),
(9, 'Mursinin Ibrahim Samosir M.Pd', 'lsinaga@hidayanto.co.id', '(+62) 318 8004 029', '2021-06-05 21:18:36'),
(10, 'Nova Amelia Yolanda S.Ked', 'tantri.novitasari@pertiwi.desa.id', '0892 1796 4344', '2021-06-05 21:18:36'),
(11, 'Mala Padmasari', 'sirait.salimah@nasyiah.co', '(+62) 581 0285 1240', '2021-06-05 21:18:36'),
(12, 'Melinda Hariyah', 'pyulianti@gmail.com', '(+62) 893 992 892', '2021-06-05 21:18:36'),
(13, 'Rina Mulyani', 'wsuartini@nashiruddin.id', '0556 0601 5253', '2021-06-05 21:18:36'),
(14, 'Hilda Mardhiyah', 'jane09@lestari.biz.id', '0773 9243 061', '2021-06-05 21:18:36'),
(15, 'Nadine Winda Suryatmi', 'woktaviani@sitorus.my.id', '0294 2300 2447', '2021-06-05 21:18:36'),
(16, 'Vivi Titin Usamah S.Kom', 'pratiwi.kania@tampubolon.go.id', '(+62) 801 0130 120', '2021-06-05 21:18:36'),
(17, 'Gading Nainggolan', 'waskita.murti@yahoo.co.id', '(+62) 565 6123 517', '2021-06-05 21:18:36'),
(18, 'Yunita Palastri M.Farm', 'okto82@gmail.co.id', '0576 4411 4300', '2021-06-05 21:18:36'),
(19, 'Vivi Mardhiyah', 'anom80@mandala.tv', '(+62) 469 6770 264', '2021-06-05 21:18:36'),
(20, 'Umay Hidayat S.Ked', 'suryatmi.nugraha@gmail.com', '0245 1592 893', '2021-06-05 21:18:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact_app`
--
ALTER TABLE `contact_app`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact_app`
--
ALTER TABLE `contact_app`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
